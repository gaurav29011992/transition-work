#!/usr/bin/env bash

# author: Gaurav Dhingra, Shiva Cheedella
# date: Nov 8, 2018
# Purpose:


theatrobizDatabase=theatrobiz
theatroStatDatabase=theatrostat_new
theatroQRDatabase=QuickReport

LOGFILE="/var/log/theatro/$(basename $0).log"
rm -rf $LOGFILE
touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }
concatsymbol=','
RET_CODE=0

# Params to be changed per deployment (ie. once)
  User=theatro
  Password=NewReportingServer!
  Hostname=127.0.0.1
export MYSQL_PWD=$Password
mysql="mysql -u$User -h$Hostname"


TODAY_DATE=`date --date="0 day ago" +"%Y-%m-%d"`
ACTUAL_DATE=`date --date="1 day ago" +"%Y-%m-%d"`

macro_mrbCounts()
{


      xPDate=
      xHour=
      xCount=


    local def=$($mysql $theatrobizDatabase -N -e "SELECT concat(cast(now() as Date),'$concatsymbol',hour(now()),'$concatsymbol',count(*))
     FROM DeviceManagement where status = 1 and storeId = 1019")


     if [[ ! -z $def ]] # means
             then

       xPDate="$(echo "$def" | cut -d',' -f1)"
        xHour="$(echo "$def" | cut -d',' -f2)"
        xCount="$(echo "$def" | cut -d',' -f3)"


       printf " Present Date is $xPDate, Hour is $xHour, Count is $xCount  \n" | tee -a $LOGFILE
               $mysql  -D"$theatroQRDatabase" -e "
              start transaction;
                 insert into MRB_Count values ('$xPDate',$xHour,$xCount);
                 commit;"


             else
       printf "Failed to get data .. \n" | tee -a $LOGFILE
             fi


}





function main()
{
printf "\n-------------------------------STARTED AT `date` ---------------.\n\n" | tee -a $LOGFILE

  macro_mrbCounts 2>>$LOGFILE
printf "\n-------------------------------ENDED AT `date` ---------------.\n\n" | tee -a $LOGFILE
}


main


