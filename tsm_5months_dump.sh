#!/usr/bin/env bash

# author: Gaurav Dhingra, Shiva Cheedella
# date: Sep 11,2018

# Purpose: cull  5 months of data for Gaurav & Sujit to do Stephen's Regression Model Requirement


END_TIME=$(date +%s)
HOWLONGINONEDAY=86400 # 24 hr
START_TIME=$((END_TIME-HOWLONGINONEDAY))

theatrobizDatabase=theatrobiz
theatroQRDatabase=QuickReport

LOGFILE="/var/log/theatro/$(basename $0).log"
rm -rf $LOGFILE
touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }
L_BACKUP_TIME=$(date +%Y-%m-%d-%H-%M-%S)
concatsymbol=','
RET_CODE=0

# Params to be changed per deployment (ie. once)
  User=theatro
  Password=NewReportingServer!
  Hostname=127.0.0.1
export MYSQL_PWD=$Password
mysql="mysql -u$User -h$Hostname"

#ACTUAL_DATA_DATE=`date --date="0 days ago" +"%Y-%m-%d"`
START_DATE="2018-04-1"
END_DATE="2018-09-11"

FILE=


#if [ $# -ne 1 ]
#then
#        echo -e "\nERROR: Incorrect arguments passed.."
#        echo -e "\nUSAGE: sh ${0} <hr number> "
#        exit 0
#fi

hr_of_the_day=11



# Observation -> When a table is dumped out especially the tsm_data and populated into tsm_data_small (the id seems to be zero, auto increment id)

# this is a one time, done offline, just having this for info
#prepareTmpTable()
#{
#
#printf "\n-------preparing temporary table it..  `date` ---------------.\n\n" | tee -a $LOGFILE
#
#$mysql $theatroQRDatabase -N -e "create table tsm_data_small like tsm_data";
#
#printf "\n-------completed with creation of temporary table.. `date` ---------------.\n\n" | tee -a $LOGFILE
#
#
#}

populateTableFromTSMData()
{


# delete the previous runs files
rm -rf /var/lib/mysql/tsm_5_months.csv


#            $mysql $theatroQRDatabase -e "
#            start transaction;
#            delete from tsm_data_small where date_id='$ACTUAL_DATA_DATE';
#            commit;"



printf "\n-------------------------------carving data from tsm_data AT `date` ---------------.\n\n" | tee -a $LOGFILE

# carve out the data from tsm_data and write into a file
      $mysql -D"$theatroQRDatabase" -Ns -e "SELECT * FROM tsm_data where date_id > '$START_DATE'  and date_id < '$END_DATE' and hour in ($hr_of_the_day) INTO OUTFILE '/var/lib/mysql/tsm_5_months.csv'"

printf "\n-------------------------------completed carving data from tsm_data AT `date` ---------------.\n\n" | tee -a $LOGFILE



printf "\n-------------------------------loading the data into tsm_data_small AT `date` ---------------.\n\n" | tee -a $LOGFILE

# load the data into the short table
      $mysql -D"$theatroQRDatabase" -Ns -e "set foreign_key_checks=0 ; LOAD DATA LOCAL INFILE '/var/lib/mysql/tsm_5_months.csv' IGNORE INTO TABLE tsm_temporary_gaurav" >> $LOGFILE 2>&1

printf "\n-------------------------------completed loading the data into tsm_data_small AT `date` ---------------.\n\n" | tee -a $LOGFILE



echo ""

}



function runGauravsQuery ()
{
echo ""
#pending
# indexes to be added to tsm_temporary_gaurav, so that queries are faster..
# CREATE INDEX idx_tsm_temporary_gaurav_date ON tsm_temporary_gaurav (date_id); and same thing was done for chain & store column as well
# run gaurav's query

      $mysql -D"$theatroQRDatabase" -Ns -e "
create table QuickReport.Regression_Data as
 SELECT chain,store,date_id,
 COUNT(IF(days_offline < 1 and online = True and live = True and assigned_not_seen = False and hour = 11, 1, NULL)) as Online_Devices,
 COUNT(IF(online = False and live = True and assigned_not_seen = True and hour = 11, 1, NULL)) as Assigned_Not_Seen,
 COUNT(IF(days_offline < 1 and online = False and live = True and assigned_not_seen = False and hour = 11, 1, NULL)) as Offline_lt_1day,
 COUNT(IF(days_offline >= 1 and days_offline < 14 and online = False and live = True and assigned_not_seen = False and hour = 11, 1, NULL)) as Offline_gt_1_and_lt_14days,
 COUNT(IF(days_offline >= 14 and days_offline < 28 and online = False and live = True and assigned_not_seen = False and hour = 11, 1, NULL)) as Offline_gt_14_and_lt_28days,
 COUNT(IF(days_offline >= 28 and days_offline < 42 and online = False and live = True and assigned_not_seen = False and hour = 11, 1, NULL)) as Offline_gt_28_and_lt_42days,
 COUNT(IF(days_offline >= 42 and online = False and live = True and assigned_not_seen = False and hour = 11, 1, NULL)) as Offline_gt_42days
 FROM QuickReport.tsm_temporary_gaurav
 where live = True
 GROUP BY chain,store,date_id"


}


function main()
{
printf "\n-------------------------------STARTED AT `date` ---------------.\n\n" | tee -a $LOGFILE

  # populateTableFromTSMData 2>>$LOGFILE
   runGauravsQuery 2>>$LOGFILE

printf "\n-------------------------------ENDED AT `date` ---------------.\n\n" | tee -a $LOGFILE
}


main



