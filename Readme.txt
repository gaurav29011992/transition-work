Field Hardware Report Scripts –

Tableau Scripts which pull the data from the Tableau Server and adds the date to the file name.
There are 3 scripts for each time snapshot.

MRB Scripts 
This was the activity carried out by the Operations Team in the past where they wanted to track the MRB Devices which were coming online in the stores or are returned to the Theatro.
Script basically checks for the devices coming online in the TSM and as well as the BAPS to see if they were returned to Theatro.

Non-Fused Online:
Script was written to extract the Non fused devices which are online in the stores so that the support team can investigate and turn them off remotely or ask the store manager to return it back to Theatro.

Offline Script – Tier 1 Team Reporting
Tableau Scripts to determine the priority call list for the stores. The template is stored on the Tableau Server which gets refreshed every day.

Weekly Pull Scripts
There are two scripts corresponding to this data request. First is the Tableau Script which automates the process of generating the necessary excel files. Template is stored on the Tableau Server.
Once the excel files are generated there is a Python Script which merges all the excel files in one and do some formatting to generate the final output.

Command Acceptance
Script was created to generate command acceptance percentage at a store level from the raw data (Speech Recognition)

Recovery Dashboard Data
Script was created to pull device Metrics, Peak Users and Shipping/Return Data in one single table. There are two scripts regarding this. One runs in the morning and the other one runs in the evening.
TSM Dump for 15 days
Script was created for a short tsm dump for the last 15 days. The intention behind that was to make the querying to the TSM_Data faster.

UnAssigned Devices List
Script was created to classify the returned devices in different categories on the basis of the number of days they were offline in the store before they were shipped back to Theatro. Mostly this data is used in the Tiger Team Dashboard 

