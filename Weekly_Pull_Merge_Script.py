import pandas as pd

print("Writing to excel sheet")

fname = '/home/airflow_projects/Ship_Return_C2_Metrics/'+'Weekly_Pull.xlsx'

print("fname of output", fname)

writer = pd.ExcelWriter(fname, engine='xlsxwriter')

df1 = pd.read_csv("/home/airflow_projects/Ship_Return_C2_Metrics/Weekly_Pull/C2_Metrics.csv",index_col=False)

df1 = df1.rename(columns={' Date ':'Month, Day, Year of Date',' Measure Names ':'Metrics',' Measure Values':'Count'})

#df1.applymap(lambda x:x.strip())

df1['Month, Day, Year of Date'] = df1['Month, Day, Year of Date'].str.strip()

#df1.columns.str.strip()

df1[' Store '] = df1[' Store '].str.strip()

df1['Metrics'] = df1['Metrics'].str.strip()

df1['Chain '] = df1['Chain '].str.strip()

df1['Count'] = df1['Count'].astype(str)

df1['Count'] = df1['Count'].str.strip()

df1['Count'] = pd.to_numeric(df1['Count'])


df1['Month, Day, Year of Date'] = pd.to_datetime(df1['Month, Day, Year of Date'],format = "%m/%d/%Y")

df2 = pd.read_csv("/home/airflow_projects/Ship_Return_C2_Metrics/Weekly_Pull/Shipping_Return_Metrics.csv",index_col=False)

df2 = df2.rename(columns={' Week_Number ':'Week of Happen Date',' Last_Day_of_the_Week ':'Month, Day, Year of Last Day of the Week',' Action ':'Action1',' Board_IDs':'Count'})

df2['Week of Happen Date'] = 'Week '+df2['Week of Happen Date'].astype(str)

#df2[" Week_Number "] = pd.to_numeric(df2[" Week_Number "],errors='coerce')

df2['Month, Day, Year of Last Day of the Week'] = df2['Month, Day, Year of Last Day of the Week'].str.strip()

df2[' Store '] = df2[' Store '].str.strip()

df2['Action1'] = df2['Action1'].str.strip()

df2[' Status '] = df2[' Status '].str.strip()

df2['Chain '] = df2['Chain '].str.strip()

df2['Week of Happen Date'] = df2['Week of Happen Date'].str.strip()

df2['Count'] = df2['Count'].astype(str).str.strip()

df2['Count'] = pd.to_numeric(df2['Count'])

df2['Month, Day, Year of Last Day of the Week'] = pd.to_datetime(df2['Month, Day, Year of Last Day of the Week'],format = "%m/%d/%Y")

#df2.applymap(lambda x:x.strip())

df3 = pd.read_csv("/home/airflow_projects/Ship_Return_C2_Metrics/Weekly_Pull/Peak_Users_Last_Sunday.csv",index_col=False)

df3 = df3.rename(columns={' Date ':'Month, Day, Year of Date',' Measure Names ':'Metrics',' Measure Values':'Count'})

df3.columns.str.strip()

#df3['Count'] = pd.to_numeric(df3['Count'],errors='coerce')

df3[' Store '] = df3[' Store '].str.strip()

df3['Month, Day, Year of Date'] = df3['Month, Day, Year of Date'].str.strip()

df3['Metrics'] = df3['Metrics'].str.strip()

df3['Chain '] = df3['Chain '].str.strip()

df3['Count'] = df3['Count'].astype(str).str.strip()

df3['Count'] = pd.to_numeric(df3['Count'])


df3['Month, Day, Year of Date'] = pd.to_datetime(df3['Month, Day, Year of Date'],format = "%m/%d/%Y")

df1.to_excel(writer, sheet_name='C2_Metrics',index=False)

df2.to_excel(writer, sheet_name='Shipment_Return',index=False)

df3.to_excel(writer, sheet_name='Peak_Users_Last_Sunday',index=False)


#print(df1.columns)

#print(df2.columns)

writer.save()

