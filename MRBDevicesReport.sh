#!/usr/bin/env bash

# author: Gaurav Dhingra, Shiva Cheedella
# date: Nov 8, 2018
# Purpose:
# DB Table Schema: Gaurav already added them manually on Reporting Server (10.0.0.57) in QuickReport


theatrobizDatabase=theatrobiz
theatroStatDatabase=theatrostat_new
theatroQRDatabase=QuickReport

LOGFILE="/var/log/theatro/$(basename $0).log"
rm -rf $LOGFILE
touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }
concatsymbol=','
RET_CODE=0
ACTUAL_DATA_DATE=`date --date="0 days ago" +"%Y-%m-%d"`

# Params to be changed per deployment (ie. once)
  User=theatro
  Password=NewReportingServer!
  Hostname=127.0.0.1
export MYSQL_PWD=$Password
mysql="mysql -u$User -h$Hostname"


TODAY_DATE=`date --date="0 day ago" +"%Y-%m-%d"`
ACTUAL_DATE=`date --date="1 day ago" +"%Y-%m-%d"`

# devices that showed up at Theatro and went thru BAPS but were part of MRB list (basically written off list)
usecase1()
{

 while read cBoardID
    do

     xcompanyId=
      xStoreID=
      xChain=
      xStore=


# this is to get the previous stores where the device was supposed to be
    local def=$($mysql $theatroStatDatabase -N -e "select concat(companyId,'$concatsymbol',storeId) from DeviceManagementAudit_Day where Action = 'UNASSIGN' and
      storeId NOT IN (109,1019) and BoardId='$cBoardID' group by companyId,storeId order by max(happen_date) desc limit 1")


     if [[ ! -z $def ]] # means
             then
     date_born=$($mysql $theatroStatDatabase -N -e "select min(happen_date) from theatrostat_new.DeviceManagementAudit_Day where Action = 'ADD' and BoardId='$cBoardID'")

       xcompanyId="$(echo "$def" | cut -d',' -f1)"
        xStoreID="$(echo "$def" | cut -d',' -f2)"


       printf "Date born is $date_born, Today's date $TODAY_DATE MRB Device Seen at Theatro, BoardID is $cBoardID, Previous Actual Company ID $xcompanyId, Previous Actual Store ID $xStoreID  \n" | tee -a $LOGFILE
               $mysql  -D"$theatroQRDatabase" -e "
              start transaction;
                 insert into MRB_Returns values ('$cBoardID','$xcompanyId','$xStoreID','$date_born','$TODAY_DATE');
                 commit;"


             else
       printf " usecase1 -> $cBoardID..  \n" | tee -a $LOGFILE
             fi

   # done< <($mysql -D"$theatroQRDatabase" -Ns -e "select A.BoardID from QuickReport.MRB_BoardIDs as A inner join
   # theatrostat_new.DeviceManagementAudit_Day as B ON A.BoardID = B.BoardId where B.Action = 'UNASSIGN' and B.storeId = 1019")

  done< <($mysql -D"$theatroQRDatabase" -Ns -e "select BoardID from  
    theatrostat_new.DeviceManagementAudit_Day where Action = 'UNASSIGN' and storeId = 1019")
}



# devices that came online in a store and hence visible on TSM as a result  but were part of MRB list  (basically written off list)
usecase2()
{

 while read cBoardID
    do

     xcompanyId=
      xStoreID=
      xChain=
      xStore=
      xCVer=

    local abc=$($mysql $theatroQRDatabase -N -e "select concat(Chain,'$concatsymbol',store,'$concatsymbol',Cver) from tsm_data_small where date_id = '$ACTUAL_DATA_DATE' 
      and online = True and board_id='$cBoardID' limit 1")


         printf " usecase2 first if-> $abc ..  \n" | tee -a $LOGFILE

     if [[ ! -z $abc ]] # means
             then


         xChain="$(echo "$abc" | cut -d',' -f1)"
       xStore="$(echo "$abc" | cut -d',' -f2)"
       xCVer="$(echo "$abc" | cut -d',' -f3)"
       printf "MRB Device Seen at a Real Store and there by showed up in TSM, BoardID is $cBoardID , Company Name the device seen is $xChain, Store Name the device seen is $xStore , cversion is $xCVer \n" | tee -a $LOGFILE
     date_born=$($mysql $theatroStatDatabase -N -e "select min(happen_date) from theatrostat_new.DeviceManagementAudit_Day where Action = 'ADD' and BoardId='$cBoardID'")

               $mysql  -D"$theatroQRDatabase" -e "
                 start transaction;
                 insert into MRB_Online values ('$cBoardID','$xChain','$xStore','$date_born','$TODAY_DATE','$xCVer');
                 commit;"
             else
         printf " usecase2 -> $cBoardID..  \n" | tee -a $LOGFILE

             fi


   #done< <($mysql -D"$theatroQRDatabase" -Ns -e "select BoardID from MRB_BoardIDs")

 done< <($mysql -D"$theatroQRDatabase" -Ns -e "select BoardID from 
    theatrobiz.DeviceManagement where storeId = 1019 and status=1")

}


function main()
{
printf "\n-------------------------------STARTED AT `date` ---------------.\n\n" | tee -a $LOGFILE

  usecase1 2>>$LOGFILE
  usecase2 2>>$LOGFILE
printf "\n-------------------------------ENDED AT `date` ---------------.\n\n" | tee -a $LOGFILE
}


main


