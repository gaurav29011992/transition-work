#!/usr/bin/env bash

# author: Gaurav Dhingra, Shiva Cheedella
# date: July 02,2018

# Purpose: cull only 15 days of data and persist in a smaller table to be used by reports for Gaurav


END_TIME=$(date +%s)
HOWLONGINONEDAY=86400 # 24 hr
START_TIME=$((END_TIME-HOWLONGINONEDAY))

theatrobizDatabase=theatrobiz
#theatroStatDatabase=theatrostat_new
theatroQRDatabase=QuickReport

LOGFILE="/var/log/theatro/$(basename $0).log"
rm -rf $LOGFILE
touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }
L_BACKUP_TIME=$(date +%Y-%m-%d-%H-%M-%S)
concatsymbol=','
RET_CODE=0

# Params to be changed per deployment (ie. once)
  User=theatro
  Password=NewReportingServer!
  Hostname=127.0.0.1
export MYSQL_PWD=$Password
mysql="mysql -u$User -h$Hostname"

ACTUAL_DATA_DATE=`date --date="0 days ago" +"%Y-%m-%d"`
FILE=


if [ $# -ne 1 ]
then
        echo -e "\nERROR: Incorrect arguments passed.."
        echo -e "\nUSAGE: sh ${0} <hr number> "
        exit 0
fi

hr_of_the_day=${1}



# Observation -> When a table is dumped out especially the tsm_data and populated into tsm_data_small (the id seems to be zero, auto increment id)

# this is a one time, done offline, just having this for info
#prepareTmpTable()
#{
#
#printf "\n-------preparing temporary table it..  `date` ---------------.\n\n" | tee -a $LOGFILE
#
#$mysql $theatroQRDatabase -N -e "create table tsm_data_small like tsm_data";
#
#printf "\n-------completed with creation of temporary table.. `date` ---------------.\n\n" | tee -a $LOGFILE
#
#
#}

populateTableFromTSMData()
{


# delete the previous runs files
rm -rf /var/lib/mysql/tsm_data_small.csv


#            $mysql $theatroQRDatabase -e "
#            start transaction;
#            delete from tsm_data_small where date_id='$ACTUAL_DATA_DATE';
#            commit;"



printf "\n-------------------------------carving data from tsm_data AT `date` ---------------.\n\n" | tee -a $LOGFILE

# carve out the data from tsm_data and write into a file
      $mysql -D"$theatroQRDatabase" -Ns -e "SELECT * FROM tsm_data where date_id='$ACTUAL_DATA_DATE' and hour in ($hr_of_the_day) INTO OUTFILE '/var/lib/mysql/tsm_data_small.csv'"

printf "\n-------------------------------completed carving data from tsm_data AT `date` ---------------.\n\n" | tee -a $LOGFILE



printf "\n-------------------------------loading the data into tsm_data_small AT `date` ---------------.\n\n" | tee -a $LOGFILE

# load the data into the short table
      $mysql -D"$theatroQRDatabase" -Ns -e "set foreign_key_checks=0 ; LOAD DATA LOCAL INFILE '/var/lib/mysql/tsm_data_small.csv' IGNORE INTO TABLE tsm_data_small" >> $LOGFILE 2>&1

printf "\n-------------------------------completed loading the data into tsm_data_small AT `date` ---------------.\n\n" | tee -a $LOGFILE

# todo - if failed to load data send out an email

echo ""

}






function main()
{
printf "\n-------------------------------STARTED AT `date` ---------------.\n\n" | tee -a $LOGFILE

   populateTableFromTSMData 2>>$LOGFILE

printf "\n-------------------------------ENDED AT `date` ---------------.\n\n" | tee -a $LOGFILE
}


main


