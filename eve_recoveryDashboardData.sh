#!/usr/bin/env bash

# author: Gaurav Dhingra, Shiva Cheedella
# date: June 24,2018

# Purpose: Create Hardware Repair Center Dashboard



END_TIME=$(date +%s)
HOWLONGINONEDAY=86400 # 24 hr
START_TIME=$((END_TIME-HOWLONGINONEDAY))

theatrobizDatabase=theatrobiz
theatroStatDatabase=theatrostat_new
theatroQRDatabase=QuickReport

LOGFILE="/var/log/theatro/$(basename $0).log"
rm -rf $LOGFILE
touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }
FILEPATH="//var/log/theatro/staging/hightempreports"
L_BACKUP_TIME=$(date +%Y-%m-%d-%H-%M-%S)
concatsymbol=','
RET_CODE=0

# Params to be changed per deployment (ie. once)
  User=theatro
  Password=NewReportingServer!
  Hostname=127.0.0.1
export MYSQL_PWD=$Password
mysql="mysql -u$User -h$Hostname"


TODAY_DATE=`date --date="0 day ago" +"%Y-%m-%d"`

#  -> date to be programmitcally done for each day its been run
#  nice to have -> contracted from configparams instead of BSD2 (nice to have)
# nice to have -> optimizations


populateDataFromBSD2PerDay()
{

printf "\n-------Populating Data from BasicStoreDetails2 for the day: `date` ---------------.\n\n" | tee -a $LOGFILE


$mysql $theatroQRDatabase -N -e "INSERT INTO RecoveryDashboardTable (Chain, Store,Date,hour,live,C2_Online,c2_offline,contracted,assigned_not_seen,lost)
    select Chain, Store,Date,hour,live,C2_Online,c2_offline,contracted,assigned_not_seen,lost from QuickReport.BasicStoreDetails2 where Date='$TODAY_DATE 00:00:00' and hour=8 and live=1";

printf "\n-------completed with creation of temporary table and population.. `date` ---------------.\n\n" | tee -a $LOGFILE


}

getChainID ()
{

#select a.CompanyID, a.Name, b.StoreID, b.Name,c.Chain,c.Store from theatrobiz.companies a, theatrobiz.stores b, QuickReport.RecoveryDashboardTable c where a.Name=c.Chain and b.Name=c.Store and c.Date='$TODAY_DATE 00:00:00' and b.markAsDelete='N' and a.markAsDelete='N'
#       read xCompanyName < <($mysql $theatrobizDatabase -N -e "select name from companies where companyId=$xcompanyId")
#       read xStoreName < <($mysql $theatrobizDatabase -N -e "select name from stores where storeId=$xstoreId")
#


# get the companies id using the below
#select distinct(a.CompanyID),a.Name from theatrobiz.companies a, QuickReport.RecoveryDashboardTable b where a.Name= b.Chain and a.markAsDelete='N'

  while read cData
    do

      xcompanyId=
      xCompanyName=

       IFS=$concatsymbol  read  xcompanyId xCompanyName  <<< "$cData"


$mysql $theatroQRDatabase -N -e "update QuickReport.RecoveryDashboardTable set companyId=$xcompanyId where Chain='$xCompanyName' and Date='$TODAY_DATE 00:00:00'";


   done< <($mysql -D"$theatroQRDatabase" -Ns -e "select distinct(concat(a.CompanyID,'$concatsymbol',a.Name)) from theatrobiz.companies a, QuickReport.RecoveryDashboardTable b where a.Name= b.Chain and a.markAsDelete='N'
")





}


getStoreID()
{


# get the store ids using the below
#select concat(a.StoreID,',',b.Chain,',',a.Name) from theatrobiz.stores a, RecoveryDashboardTable b where a.Name= b.Store and a.markAsDelete='N' and b.companyId=a.CompanyID


 while read cData1
    do

      xStoreId1=
      xCompanyName1=
      xStoreName1=

       IFS=$concatsymbol  read  xStoreId1 xCompanyName1 xStoreName1 <<< "$cData1"


$mysql $theatroQRDatabase -N -e "update QuickReport.RecoveryDashboardTable set storeId=$xStoreId1 where Chain='$xCompanyName1' and Store='$xStoreName1' and Date='$TODAY_DATE 00:00:00'";


   done< <($mysql -D"$theatroQRDatabase" -Ns -e "select concat(a.StoreID,'$concatsymbol',b.Chain,'$concatsymbol',a.Name) from theatrobiz.stores a, QuickReport.RecoveryDashboardTable b where a.Name= b.Store and a.markAsDelete='N' and b.companyId=a.CompanyID")



}

# little bit expensive function -> around 30 secs for 568 stores
updateDataFromDeviceManagement()
{

# select count(*) from theatrobiz.DeviceManagement where storeId=26 and status=1

      xStoreId=

 while read xStoreId
    do


         read xAssigned < <($mysql $theatroQRDatabase -N -e "select count(*) from theatrobiz.DeviceManagement where storeId=$xStoreId and status=1")

$mysql $theatroQRDatabase -N -e "update QuickReport.RecoveryDashboardTable set c2s_assigned=$xAssigned where storeId=$xStoreId  and Date='$TODAY_DATE 00:00:00'";


   done< <($mysql -D"$theatroQRDatabase" -Ns -e "select storeId from QuickReport.RecoveryDashboardTable where Date='$TODAY_DATE 00:00:00'")



}

updateDataFromTSM()
{
#  -> create the c2_offline less than a day from TSM instead of BSD2
#  -> create the lost from TSM (for consistency) instead of BSD2
#select count(distinct(board_id)) from QuickReport.tsm_data where Chain='Cabelas' and Store='Allen_35' and hour=8 and live=1 and date_id='$TODAY_DATE' and days_offline >=1 and days_offline < 7


while read cData1
    do

      xChain=
      xStore=
      xDate=

       IFS=$concatsymbol  read  xChain xStore xDate <<< "$cData1"


         read c2s_offline_gt1_lt6 < <($mysql $theatroQRDatabase -N -e "select count(distinct(board_id)) from QuickReport.tsm_data where Chain='$xChain' and Store='$xStore'
          and hour=8 and live=1 and date_id=cast('$xDate' as Date) and days_offline >=1 and days_offline < 7 and assigned_not_seen=0")

         read lost < <($mysql $theatroQRDatabase -N -e "select count(distinct(board_id)) from QuickReport.tsm_data where Chain='$xChain' and Store='$xStore'
          and hour=8 and live=1 and date_id=cast('$xDate' as Date) and days_offline >= 7 and assigned_not_seen=0")


         read lessthan_one_day_offline < <($mysql $theatroQRDatabase -N -e "select count(distinct(board_id)) from QuickReport.tsm_data where Chain='$xChain' and Store='$xStore'
          and hour=8 and live=1 and date_id=cast('$xDate' as Date) and days_offline < 1 and online=false and assigned_not_seen=0")

        $mysql $theatroQRDatabase -N -e "update QuickReport.RecoveryDashboardTable set c2s_offline_gt1_lt6=$c2s_offline_gt1_lt6,lost=$lost,c2_offline=$lessthan_one_day_offline where Chain='$xChain' and Store='$xStore'  and Date='$xDate'";




   done< <($mysql -D"$theatroQRDatabase" -Ns -e "select concat(Chain,'$concatsymbol',Store,'$concatsymbol',Date) from QuickReport.RecoveryDashboardTable where Date='$TODAY_DATE 00:00:00'")



}


updateDataFromDeviceManagementAudit()
{


#
#select count(distinct boardId) as Assigned_Devices
#from theatrostat_new.DeviceManagementAudit
#where Action = "Assign" and companyId=$xCompanyId and storeId=$xStoreId
#and cast(from_unixtime(substr(updatedDate,1,10)) as Date) = cast('$xDate' as Date)
#
#
#
#select count(distinct boardId) as UnAssigned_Devices
#from theatrostat_new.DeviceManagementAudit
#where Action = "UNASSIGN" and companyId=$xCompanyId and storeId=$xStoreId
#and cast(from_unixtime(substr(updatedDate,1,10)) as Date) = cast('$xDate' as Date)

while read cData1
    do

      xCompanyId=
      xStoreId=
      xDate=

       IFS=$concatsymbol  read  xCompanyId xStoreId xDate <<< "$cData1"

#echo "start \n\n"
#echo $xCompanyId
#echo $xStoreId
#echo $xDate

         read c2s_assigned_and_shipped < <($mysql $theatroStatDatabase -N -e "select count(distinct (boardId))
from theatrostat_new.DeviceManagementAudit
where Action = 'Assign' and companyId=$xCompanyId and storeId=$xStoreId
and cast(from_unixtime(substr(updatedDate,1,10)) as Date) = cast('$xDate' as Date)")

         read c2s_returned < <($mysql $theatroStatDatabase -N -e "select count(distinct (boardId))
from theatrostat_new.DeviceManagementAudit
where Action = 'UNASSIGN' and companyId=$xCompanyId and storeId=$xStoreId
and cast(from_unixtime(substr(updatedDate,1,10)) as Date) = cast('$xDate' as Date)")

#echo "\n"
#echo $c2s_assigned_and_shipped
#echo $c2s_returned
#echo "end \n"

        $mysql $theatroQRDatabase -N -e "update QuickReport.RecoveryDashboardTable set c2s_assigned_and_shipped=$c2s_assigned_and_shipped,c2s_returned=$c2s_returned where
         companyId=$xCompanyId and storeId=$xStoreId  and Date='$xDate'";




   done< <($mysql -D"$theatroQRDatabase" -Ns -e "select concat(companyId,'$concatsymbol',storeId,'$concatsymbol',Date) from QuickReport.RecoveryDashboardTable where Date='$TODAY_DATE 00:00:00'")



}


function main()
{
printf "\n-------------------------------STARTED AT `date` ---------------.\n\n" | tee -a $LOGFILE

#   populateDataFromBSD2PerDay 2>>$LOGFILE
   getChainID 2>>$LOGFILE
   getStoreID 2>>$LOGFILE
   updateDataFromDeviceManagement 2>>$LOGFILE
 #  updateDataFromTSM 2>>$LOGFILE
   updateDataFromDeviceManagementAudit 2>>$LOGFILE

printf "\n-------------------------------ENDED AT `date` ---------------.\n\n" | tee -a $LOGFILE
}


main


