#!/usr/bin/env bash

# author: Gaurav Dhingra, Shiva Cheedella
# date: June 12,2018

# Purpose: will generate a report of BoardID's that were assigned on the day it's run.

# todo ->  -> Also, tsm_data would be only for 11.00 AM snapshot we need more snapshots? and it would be a very big table, we have to see the query timing


END_TIME=$(date +%s)
HOWLONGINONEDAY=86400 # 24 hr
START_TIME=$((END_TIME-HOWLONGINONEDAY))

theatrobizDatabase=theatrobiz
theatroStatDatabase=theatrostat_new
theatroRQDatabase=QuickReport

LOGFILE="/var/log/theatro/$(basename $0).log"
rm -rf $LOGFILE
touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }
FILEPATH="//var/log/theatro/staging/hightempreports"
L_BACKUP_TIME=$(date +%Y-%m-%d-%H-%M-%S)
concatsymbol=','
RET_CODE=0

# Params to be changed per deployment (ie. once)
  User=theatro
  Password=NewReportingServer!
  Hostname=127.0.0.1
export MYSQL_PWD=$Password
mysql="mysql -u$User -h$Hostname"




prepareAndPopulateTmpTable()
{

printf "\n-------preparing temporary table and populating it..  `date` ---------------.\n\n" | tee -a $LOGFILE


  while read cData
    do

      xcompanyId=
      xstoreId=
      xboardId=
      xoffline_days=
      xTime=

       IFS=$concatsymbol  read xboardId xstoreId xcompanyId xTime  <<< "$cData"


#       read xCompanyName < <($mysql $theatrobizDatabase -N -e "select name from companies where companyId=$xcompanyId")
#       read xStoreName < <($mysql $theatrobizDatabase -N -e "select name from stores where storeId=$xstoreId")
#


$mysql $theatroStatDatabase -N -e "INSERT INTO unAssignedDevicesList (boardId, company_name,store_name,date_info,offline_days)
    select board_id,chain,store,date_id,days_offline as Days_Offline from QuickReport.tsm_data where board_id = '$xboardId'
 and chain NOT IN ('Theatro')  order by date_id desc limit 1";


   done< <($mysql -D"$theatroStatDatabase" -Ns -e "SELECT concat(BoardId,'$concatsymbol',storeId,'$concatsymbol',companyId,'$concatsymbol',cast(from_unixtime(substr(updatedDate,1,10)) AS Date))
 FROM theatrostat_new.DeviceManagementAudit where Action = 'UNASSIGN'
 and companyId NOT IN (0,1)
 and CAST(FROM_UNIXTIME(substr(updatedDate,1,10)) AS DATE) = adddate(current_date(),0)
 order by BoardId")


printf "\n-------completed with creation of temporary table and population.. `date` ---------------.\n\n" | tee -a $LOGFILE


}



function generateReport(){

L_BACKUP_TIME=$(date +%Y-%m-%d-%H-%M-%S)

rm -rf /var/log/theatro/ReportForCalling*.csv;

read messages_count < <($mysql $theatroStatDatabase -N -e "SELECT chain_name,store_id,store_name,board_id,date_format(from_unixtime(time),'%m-%d-%Y %H:%i:%s') as 'happen_date' from tmpreport_for_support
  order by store_name INTO OUTFILE '/var/log/theatro/ReportForCalling_$L_BACKUP_TIME.csv' FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n'")

## sending email
L_BACKUP_TIME=$(date +%Y-%m-%d-%H-%M-%S)

sed -i '1s/^/ChainName,StoreID,StoreName,Board ID,Last Online Time \n /' /var/log/theatro/ReportForCalling_$L_BACKUP_TIME.csv

python2.6 /opt/theatro/cleanupscripts/call_restapi_sendmail_attachment.py "patrick@theatrolabs.com,kalim@theatrolabs.com,kerri@theatrolabs.com,analytics@theatrolabs.com,tammy@theatrolabs.com,johnn@theatrolabs.com,bre@theatrolabs.com,kranthi@theatrolabs.com,ravi@theatrolabs.com,david@theatrolabs.com" "Report for Calling Stores at '$L_BACKUP_TIME'" "last 24 hr online devices " "/var/log/theatro/ReportForCalling_$L_BACKUP_TIME.csv"

#python2.6 /opt/theatro/cleanupscripts/call_restapi_sendmail_attachment.py "analytics@theatrolabs.com" "JUst me Report for Calling Stores at '$L_BACKUP_TIME'" "last 24 hr online devices " "/var/log/theatro/ReportForCalling_$L_BACKUP_TIME.csv"



}


function main()
{
printf "\n-------------------------------STARTED AT `date` ---------------.\n\n" | tee -a $LOGFILE

    prepareAndPopulateTmpTable 2>>$LOGFILE
    #generateReport 2>>$LOGFILE

printf "\n-------------------------------ENDED AT `date` ---------------.\n\n" | tee -a $LOGFILE
}


main





