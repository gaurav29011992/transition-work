#!/usr/bin/env bash

# author: Gaurav Dhingra, Shiva Cheedella
# date: July 17,2018

# Purpose: To create a counts of accepted and not accepted per day per store, so that Guy's report can use it
# Currently - Tableau reports are running against SpeechRecognition and is very slow because of the sheer volume of data in the Table
# system engg -> currently for 600 stores having approximately 60,000 associates active per day, we seem to get around 2 million entries (give or take), meaning approx 4K per store.
# Crontab - Run this script around 10.00 am CST, so that it can create the data for the previous day

# original one
#CREATE TABLE `CommandAcceptance` (
# `row_id` int(11) NOT NULL AUTO_INCREMENT,
#  `Chain` varchar(255) NOT NULL DEFAULT '',
#  `Store` varchar(255) NOT NULL DEFAULT '',
#  `Date` datetime NOT NULL,
#  `server_id` int(11) NOT NULL,
#    `storeid` int(11) DEFAULT NULL,
#    `uniq_count_of_users` int(11) DEFAULT NULL,
#    `recognized_count` int(11) DEFAULT NULL,
#    `un_recognized_count` int(11) DEFAULT NULL,
#  PRIMARY KEY (row_id,`Chain`,`Store`,`Date`,`server_id`,`storeid`)
#) ENGINE=InnoDB DEFAULT CHARSET=utf8;

# new one (based on Gaurav's feedback for creating ASR report for TWM - Oct 9, 2018

#CREATE TABLE `CommandAcceptance` (
# `row_id` int(11) NOT NULL AUTO_INCREMENT,
#  `Chain` varchar(255) NOT NULL DEFAULT '',
#  `Store` varchar(255) NOT NULL DEFAULT '',
#  `origin_date` datetime NOT NULL,
#  `server_id` int(11) NOT NULL,
#  `store_id` int(11) DEFAULT NULL,
#  `uniq_count_of_users` int(11) DEFAULT NULL,
#  `accpt_cmd_count` int(11) DEFAULT NULL,
#  `un_recognized_count` int(11) DEFAULT NULL,
#  `total_cmd_count` int(11) DEFAULT NULL,
#  PRIMARY KEY (row_id,`Chain`,`Store`,`origin_date`,`server_id`,`store_id`)
#) ENGINE=InnoDB DEFAULT CHARSET=utf8;



#END_TIME=$(date +%s)
#HOWLONGINONEDAY=86400 # 24 hr
#START_TIME=$((END_TIME-HOWLONGINONEDAY))

theatrobizDatabase=theatrobiz
theatroStatDatabase=theatrostat_new
theatroQRDatabase=QuickReport

LOGFILE="/var/log/theatro/$(basename $0).log"
rm -rf $LOGFILE
touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }
FILEPATH="//var/log/theatro/staging/hightempreports"
L_BACKUP_TIME=$(date +%Y-%m-%d-%H-%M-%S)
concatsymbol=','
RET_CODE=0

# Params to be changed per deployment (ie. once)
  User=theatro
  Password=NewReportingServer!
  Hostname=127.0.0.1
export MYSQL_PWD=$Password
mysql="mysql -u$User -h$Hostname"


TODAY_DATE=`date --date="0 day ago" +"%Y-%m-%d"`
ACTUAL_DATE=`date --date="1 day ago" +"%Y-%m-%d"`
TODAY_DATE_TAR="$(date --date="0 day ago" +"%Y-%m-%d")_mysql_backup.tar.gz"

EXT_DIR="/opt/dbscripts/speechtemp"


#FILE="/opt/dbscripts/speechtemp/backup/var/log/theatro/tmp/theatrostat-SpeechRecognition-${TODAY_DATE}_*.csv"
echo $TODAY_DATE_TAR
echo $ACTUAL_DATE
#echo $FILE


populateTableFromStatsArchive()
{

   cleanTempData 2>>$LOGFILE

# /mnt/Remote/stats/2018-06-22_mysql_backup.tar.gz
mkdir -p /opt/dbscripts/speechtemp/

# get the archive file
cp "/mnt/Remote/stats/$TODAY_DATE_TAR" /opt/dbscripts/speechtemp/
cd /opt/dbscripts/speechtemp/
tar -xvf "$TODAY_DATE_TAR"
printf "\n-------------------------------Completed untarring the file $TODAY_DATE_TAR at `date` ---------------.\n\n" | tee -a $LOGFILE

FILE=$(echo "$EXT_DIR"/backup/var/log/theatro/tmp/*.csv | sed 's/\ /\n/g' | grep -i "SpeechRecognition-" | head -1)

printf "\n-------------------------------File to be loaded is $FILE ---------------.\n\n" | tee -a $LOGFILE


# prepare temporary table
    $mysql -D"$theatroStatDatabase" -Ns -e "DROP TABLE IF EXISTS tmp_SpeechRecognition"

      $mysql -D"$theatroStatDatabase" -Ns -e "CREATE TABLE tmp_SpeechRecognition like SpeechRecognition"

      $mysql -D"$theatroStatDatabase" -Ns -e "ALTER IGNORE TABLE tmp_SpeechRecognition ADD INDEX partialcolumns1 (server_id)"

# populate the data
printf "\n-------------------------------Loading data AT `date` ---------------.\n\n" | tee -a $LOGFILE
      $mysql -D"$theatroStatDatabase" -Ns -e "set foreign_key_checks=0 ; LOAD DATA LOCAL INFILE '$FILE' IGNORE INTO TABLE tmp_SpeechRecognition" >> $LOGFILE 2>&1

printf "\n-------------------------------Completed Loading data AT `date` ---------------.\n\n" | tee -a $LOGFILE

}

cleanTempData()
{
rm -rf /opt/dbscripts/speechtemp/*


}

   #    IFS=$concatsymbol  read  xStoreId1 xCompanyName1 xStoreName1 <<< "$cData1"
   #  read abc < <($mysql $theatrobizDatabase -N -e "select concat(CompanyID,'$concatsymbol',StoreID) from serverinfo where serverid=$cServerID")

generateDataAndPopulate()
{

 while read cServerID
    do

      xcompanyId=
      xStoreID=
      xCompanyName=
      xStoreName=
#     xUsersCount=
#      x_recognizedCount=
#      x_not_recognizedCount=


    local abc=$($mysql $theatrobizDatabase -N -e "select concat(CompanyID,'$concatsymbol',StoreID) from serverinfo where serverid=$cServerID")

        xcompanyId="$(echo "$abc" | cut -d',' -f1)"
        xStoreID="$(echo "$abc" | cut -d',' -f2)"


    local def=$($mysql $theatrobizDatabase -N -e "select concat(c.name,'$concatsymbol',s.name) from companies c, stores s where c.companyid=$xcompanyId and s.storeId=$xStoreID")

        xCompanyName="$(echo "$def" | cut -d',' -f1)"
        xStoreName="$(echo "$def" | cut -d',' -f2)"

    local user_count=$($mysql $theatroStatDatabase -N -e "select count(distinct(user)) from tmp_SpeechRecognition where server_id=$cServerID")
    local x_recognizedCount=$($mysql $theatroStatDatabase -N -e "select count(*) from tmp_SpeechRecognition where server_id=$cServerID and is_recognized=1")
    local x_not_recognizedCount=$($mysql $theatroStatDatabase -N -e "select count(*) from tmp_SpeechRecognition where server_id=$cServerID and is_recognized=0")
    x_total=$((x_not_recognizedCount + x_recognizedCount))


# Date='$ACTUAL_DATE 00:00:00'

       printf "inserting for "$cServerID"  \n" | tee -a $LOGFILE

               $mysql  -D"$theatroQRDatabase" -e "
                 start transaction;
                 insert into CommandAcceptance values (NULL,'$xCompanyName','$xStoreName','$ACTUAL_DATE',$cServerID,$xStoreID,$user_count,$x_recognizedCount,$x_not_recognizedCount,$x_total);
                 commit;"


   done< <($mysql -D"$theatroStatDatabase" -Ns -e "select distinct(server_id) from tmp_SpeechRecognition")
}



function main()
{
printf "\n-------------------------------STARTED AT `date` ---------------.\n\n" | tee -a $LOGFILE

   populateTableFromStatsArchive 2>>$LOGFILE
  generateDataAndPopulate 2>>$LOGFILE

printf "\n-------------------------------ENDED AT `date` ---------------.\n\n" | tee -a $LOGFILE
}


main


