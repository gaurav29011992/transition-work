#!/usr/bin/env bash

# author: Gaurav Dhingra, Shiva Cheedella
# date: July 02,2018

# Purpose: delete data older than 15 days of data


END_TIME=$(date +%s)
HOWLONGINONEDAY=86400 # 24 hr
START_TIME=$((END_TIME-HOWLONGINONEDAY))

theatrobizDatabase=theatrobiz
#theatroStatDatabase=theatrostat_new
theatroQRDatabase=QuickReport

LOGFILE="/var/log/theatro/$(basename $0).log"
rm -rf $LOGFILE
touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }
L_BACKUP_TIME=$(date +%Y-%m-%d-%H-%M-%S)
concatsymbol=','
RET_CODE=0

# Params to be changed per deployment (ie. once)
  User=theatro
  Password=NewReportingServer!
  Hostname=127.0.0.1
export MYSQL_PWD=$Password
mysql="mysql -u$User -h$Hostname"

ACTUAL_DATA_DATE=`date --date="16 days ago" +"%Y-%m-%d"`
FILE=

#TIMECAL=$(date +%s)
#HOWLONGDAYS=7776000
#THIRTYONEDAYSTIME=$((TIMECAL-HOWLONGDAYS))

purgeDataFromTSMSmall()
{

printf "\n-------------------------------start purging data older than x days from tsm_data_small AT `date` ---------------.\n\n" | tee -a $LOGFILE

            $mysql $theatroQRDatabase -e "
            start transaction;
            delete from tsm_data_small where date_id='$ACTUAL_DATA_DATE';
            commit;"
printf "\n-------------------------------completed purging data older than x days  from tsm_data_small AT `date` ---------------.\n\n" | tee -a $LOGFILE

# todo -> send an email if failed

}






function main()
{
printf "\n-------------------------------STARTED AT `date` ---------------.\n\n" | tee -a $LOGFILE

   purgeDataFromTSMSmall 2>>$LOGFILE

printf "\n-------------------------------ENDED AT `date` ---------------.\n\n" | tee -a $LOGFILE
}


main


