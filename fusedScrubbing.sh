#!/bin/bash
# purpose: Scrub the data from tsm_data and apply filtering logic on top of it to ignore few things that will be counted into fused and non-fused
# author: shiva cheedella, Gaurav Dhingra
# date: Apr 05,2018
# Target System where this script is executed: ReportingServer (new)

LOGFILE="/var/log/theatro/$(basename $0).log"
RET_CODE=0
concatsymbol=','
theatrobizDatabase=theatrobiz
theatrostatDatabase=theatrostat_new
Hostname="127.0.0.1"
User=theatro
Password=NewReportingServer!
quickreportsDatabase=QuickReport
sandboxDatabase=sandbox

mysql="mysql -h $Hostname -P 3306 -u$User -p$Password"
DEFDATE=`date --date="1 day ago" +"%Y-%m-%d"`
#DEFDATE=`date +'%F'`
DATE="${4:-$DEFDATE}"


touch $LOGFILE || { echo "Unable to create logfile..Check permissions.."; exit 1 ; }

fusedScrub () {

 $mysql $sandboxDatabase -N -e "DROP table sandbox.Fused_NonFused";


 $mysql $sandboxDatabase -N -e "create table sandbox.Fused_NonFused as select date_id,hour,chain,store,board_id,Cver,Cvol,
 online,assigned_not_seen,days_offline,lost,live from QuickReport.tsm_data where hour = 11 and live = true and date_id = adddate(cast(current_timestamp() AS date),0)";


 $mysql $sandboxDatabase -N -e "alter table sandbox.Fused_NonFused add flag varchar(255)";

 $mysql $sandboxDatabase -N -e "update sandbox.Fused_NonFused SET sandbox.Fused_NonFused.flag = CASE WHEN sandbox.Fused_NonFused.Cver LIKE '19%' THEN 'Fused'
  WHEN sandbox.Fused_NonFused.board_id LIKE 'H%' OR
  sandbox.Fused_NonFused.board_id LIKE 'I%' OR sandbox.Fused_NonFused.board_id LIKE 'J%' THEN 'Fused' ELSE 'Non Fused' END";


}

START_TIME=$(date +%Y-%m-%d_%H-%M-%S)
printf "\n##### Script $0 started at $START_TIME....####\n" | tee -a $LOGFILE

fusedScrub

END_TIME=$(date +%Y-%m-%d_%H-%M-%S)
printf "\n##### Script $0 completed at $END_TIME ....####\n" | tee -a $LOGFILE


